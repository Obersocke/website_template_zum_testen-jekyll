$(function () {
    smoothScroll(1000);
    gamesProjectsBelt();
    projectLoad();
});

//smoothScroll function is applied from the document ready function
function smoothScroll(duration) {
    $('a[href^="#"]').on('click', function (event) {
        var target = $($(this).attr('href'));

        if (target.length) {
            event.preventDefault();
            $('html, body').animate({
                scrollTop: target.offset().top
            }, duration);
        }
    });
}


// Slides in der game Section
function gamesProjectsBelt() {
    $('.thumb-unit').click(function () {
        $('.game-projects-belt').css('left','-100%');
        $('.game-projects-container').show();
    });

    $('.game-projects-return').click(function () {
        $('.game-projects-belt').css('left', '0%');
        $('.game-projects-container').hide(800);
    });

}


function projectLoad() {

    $.ajaxSetup ({ cache: true });

    $('.thumb-unit').click(function () {

        var $this= $(this),
            newTitle = $this.find('strong').text(),
            newFolder = $this.data('folder'),
            spinner = '<div class="loader">Loading...</div>',
            newHTML = '/games/'+ newFolder +'.html';
        $('.project-load').html(spinner).load(newHTML);
        $('.project-title').text(newTitle);

    });

}










